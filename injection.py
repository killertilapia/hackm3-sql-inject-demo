import sqlite3
from contextlib import closing

from flask import Flask, g, redirect, render_template, request, url_for

# config
DB = "demo.db"
DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)
app.secret_key = "nXoE3vmhhPCVcSTyFIBhKQ"
# -----------------------------------------------


@app.route("/")
def home_route():
    """
    Home route
    """
    return render_template("index.html")


@app.route("/bad_login", methods=["GET", "POST"])
def bad_app_login():
    """
    SQL Injection vulnerable login form example
    Defeated by: " OR ""=" | " OR "a"="a";--
    :return:
    """
    error = None
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]

        if bad_log_me_in(username, password):
            return redirect(url_for("secured_page"))
        else:
            error = "Invalid username/password"

    return render_template("bad_login.html", error=error)


def bad_log_me_in(username, password):
    """
    Simplified login function; Checks username+password in database
    """
    query_str = (
        'SELECT * FROM auth_user WHERE username = "'
        + username
        + '" AND password = "'
        + password
        + '"'
    )

    print(f"DEBUG SQL: {query_str}")

    cur = g.db.execute(query_str)
    user = [dict(id=row[0], usernam=row[1], password=row[2]) for row in cur.fetchall()]

    if user:
        return True

    return False


@app.route("/good_login", methods=["GET", "POST"])
def good_app_login():
    """
    Fixed login form that defeats SQL Injection
    :return:
    """
    error = None
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]

        if good_log_me_in(username, password):
            return redirect(url_for("secured_page"))
        else:
            error = "Invalid username/password"

    return render_template("good_login.html", error=error)


def good_log_me_in(username, password):
    """
    Simply using a parameterized query for your database instead of string concatenation
    prevents SQL injection.

    test credentials: admin % qwerty123
    """
    query_str = "SELECT * FROM auth_user WHERE username = ? AND password = ?"

    print(f"DEBUG SQL: {query_str}")

    # python3-sqlite specific way to do parametrized queries
    # refer to https://docs.python.org/3/library/sqlite3.html
    cur = g.db.execute(
        query_str,
        (
            username,
            password,
        ),
    )
    user = [dict(id=row[0], username=row[1], password=row[2]) for row in cur.fetchall()]

    if user:
        return True

    return False


@app.route("/bad_search", methods=["GET", "POST"])
def bad_search():
    """
    Dumpable Search feature because of SQL Injection
    Defeated by: '' OR 'a'='a'
    WTF: '' OR 'a'='a' UNION SELECT au.'id', au.'username' as 'title', au.'password' as 'text' FROM auth_user au;
    """
    error = None
    if request.method == "POST":
        key_id = request.form.get("id") or "0"

        query_str = "SELECT * FROM entry WHERE id = " + key_id

        print(f"DEBUG SQL: {query_str}")

        cur = g.db.execute(query_str)
        entry = [dict(id=row[0], title=row[1], text=row[2]) for row in cur.fetchall()]

        return render_template("bad_search.html", entry=entry)

    return render_template("bad_search.html")


@app.route("/secured_page")
def secured_page():
    query_str = "SELECT * FROM entry"
    cur = g.db.execute(query_str)
    entries = [dict(id=row[0], title=row[1], text=row[2]) for row in cur.fetchall()]
    return render_template("wut.html", entries=entries)


# region preamble


def connect_db():
    return sqlite3.connect(app.config["DB"])


def init_db():
    """
    Also resets the database
    To use, open a python shell session and just import and run it
    > from injection import init_db
    > init_db()
    """
    with closing(connect_db()) as db:
        print("Initializing/Resetting DB")
        with app.open_resource("schema.sql", mode="r") as f:
            db.cursor().executescript(f.read())
        db.commit()

    print("Done!")


@app.before_request
def before_request():
    g.db = connect_db()


@app.teardown_request
def teardown_request(exception):
    db = getattr(g, "db", None)
    if db is not None:
        db.close()


# endregion

if __name__ == "__main__":
    app.run(debug=True)
