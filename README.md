# Hackm3 Demo for SQL Injection using Flask and SQLite

This demo is meant to educate. 

SQL Injection is a common attack vector that uses malicious SQL code for backend
database manipulation to access areas or information that was not intended.

This demo will demonstrate a login and search SQLi vulnerability.

Talk length: 30-45 minutes

# Objectives 
            
1. Show what is SQL Injection
2. Recognize and understand why it happens
3. Show how to fix it
    
# Running it locally

Requires at least Python3.11 

1. Create virtual environment; venv and poetry supported
2. Install requirements
3. `python injection.py` 

Should be running on http://127.0.0.1:5000

# Notes

Includes the demo.db file