drop table if exists entry;
create table entry (
  id integer primary key autoincrement,
  title text not null,
  text text not null
);
INSERT INTO entry(title, text) VALUES ('Title #1', 'something something');
INSERT INTO entry(title, text) VALUES ('Title #2', 'another something something');
INSERT INTO entry(title, text) VALUES ('Title #3', 'anything then something');


drop table if exists auth_user;
create table auth_user(
  id integer primary key autoincrement,
  username text not null,
  password text not null
);

INSERT INTO auth_user(username, password) VALUES ('admin', 'qwerty123');
INSERT INTO auth_user(username, password) VALUES ('jay', 'simple123');
INSERT INTO auth_user(username, password) VALUES ('user1', '123simple');
